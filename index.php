<?php
require_once "vendor/autoload.php";
require_once './app/Controllers/UserApi.php';
require_once './app/Controllers/ProductsApi.php';
require_once './app/Controllers/ProCateApi.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$userApi = new UserApi();
$proApi = new ProductsApi();
$proCateApi = new ProCatesApi();

echo json_encode(array("msg" => "Api is failed!"));