-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 03:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manashop`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `pro_id` varchar(255) NOT NULL,
  `pro_name` text NOT NULL,
  `pro_quantity` bigint(20) NOT NULL,
  `pro_categories` text NOT NULL,
  `pro_saleprice` bigint(20) NOT NULL,
  `pro_purchaseprice` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pro_id`, `pro_name`, `pro_quantity`, `pro_categories`, `pro_saleprice`, `pro_purchaseprice`) VALUES
('fruit-1', 'táo xanh úc', 123213, 'fruit', 124, 1234),
('milk-1', 'banhs dua xanh', 214124, 'milk', 132132, 131414),
('milk-2', 'banhs dua xanh', 214124, 'milk', 132132, 131414),
('pork-2', 'thit lon 300g', 14532, 'pork', 32412, 14214124);

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `proCate_ID` int(100) NOT NULL,
  `proCate_code` text NOT NULL,
  `proCate_des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`proCate_ID`, `proCate_code`, `proCate_des`) VALUES
(1, 'fruit', 'Hoa quả'),
(2, 'pork', 'Thịt lợn'),
(3, 'milk', 'Sữa'),
(4, 'cookie', 'Bánh quy'),
(5, 'cake', 'Bánh ngọt');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `password`, `date`, `role`, `address`, `created`) VALUES
(1, 'John Doe', NULL, 'johndoe@gmail.com', '123456', NULL, 'Data Scientist', 'Hanoi', '2012-06-01 02:12:30'),
(2, 'David Costa', NULL, 'sam.mraz1996@yahoo.com', 'dc@blabla', NULL, 'Apparel Patternmaker', NULL, '2013-03-03 01:20:10'),
(3, 'Todd Martell', NULL, 'liliane_hirt@gmail.com', 'leuleu', NULL, 'Accountant', NULL, '2014-09-20 03:10:25'),
(4, 'Adela Marion', NULL, 'michael2004@yahoo.com', 'moeocchoa', NULL, 'Shipping Manager', NULL, '2015-04-11 04:11:12'),
(5, 'Matthew Popp', NULL, 'krystel_wol7@gmail.com', 'tatatatata', NULL, 'Chief Sustainability Officer', NULL, '2016-01-04 05:20:30'),
(6, 'Alan Wallin', NULL, 'neva_gutman10@hotmail.com', 'tatatatatat', NULL, 'Chemical Technician', NULL, '2017-01-10 06:40:10'),
(7, 'Joyce Hinze', NULL, 'davonte.maye@yahoo.com', 'tatatatatat', NULL, 'Transportation Planner', NULL, '2017-05-02 02:20:30'),
(8, 'Donna Andrews', NULL, 'joesph.quitz@yahoo.com', 'leuleu', NULL, 'Wind Energy Engineer', NULL, '2018-01-04 05:15:35'),
(9, 'Andrew Best', NULL, 'jeramie_roh@hotmail.com', 'daylamatkhau', NULL, 'Geneticist', NULL, '2019-01-02 02:20:30'),
(10, 'Joel Ogle', '0938081681', 'summer_shanah@hotmail.com', 'khongmatkhau', NULL, 'Space Sciences Teacher', NULL, '2020-02-01 06:22:50'),
(61, 'Trần Đức Nhân', '0398081681', 'nhan.itptit@gmail.com', '$2y$11$xaI1vq7O0QKomoEmBzsxWeH6pAvd6xudU00/ePx6m04j6ommqdxkK', '1999-10-06', 'admin', 'Hanoi', '2020-07-01 14:11:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`proCate_ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `proCate_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
