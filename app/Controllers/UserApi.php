<?php

require_once 'app/Utils/RestfulAPI.php';
require_once 'app/config/Database.php';
require_once 'app/Models/User.php';

class UserApi extends RestFul_API {

    private $database;
    private $db;
    private $user;

    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->user = new User($this->db);
        parent::__construct();
    }

    function user()
    {
        if ($this->method == "GET") {
            $UserID = isset($_GET["id"]) ? $_GET["id"] : null;
            if ($UserID) {
                $this->user->setID($UserID);
                $userArr = $this->user->getSingleUser();
                if (is_array($userArr)) {
                    $this->response(200, $userArr);
                } else
                    $this->response(404, $userArr);
            }
            $userArr = $this->user->getUsers();
            if (is_array($userArr)) {
                $this->response(200, $userArr);
            } else
                $this->response(404, $userArr);
        };
    }

    function signUp()
    {
        if ($this->method == "POST") {
            $data = $_POST;
            if (!isset($data["name"]))
                $this->response(500, array("error" => "No name provided."));

            else if (!isset($data["email"]))
                $this->response(500, array("error" => "No email provided."));

            else if (!isset($data["password"]))
                $this->response(500, array("error" => "No password provided."));

            else if (!isset($data["phone"]))
                $this->response(500, array("error" => "No phone number provided."));

            else if (!isset($data["role"]))
                $this->response(500, array("error" => "No role provided."));            
            
            $this->user->setName($data["name"]);
            $this->user->setPhone($data["phone"]);
            $this->user->setEmail($data["email"]);
            $this->user->setPassword($data["password"]);
            $this->user->setDate(date("Y-m-d", strtotime($data["date"])));
            $this->user->setRole($data["role"]);
            $this->user->setAddress($data["address"]);
            $this->user->setCreated(date('Y-m-d H:i:s'));
            
            $result = $this->user->createUser();
            $this->response($result["status"], $result["msg"]);
        }
    }

    function login()
    {
        if ($this->method == "POST") {
            $data = $_POST;

            if ($data["email"] == null)
                $this->response(500, array("error" => "No Email provided."));
            else if ($data["password"] == null)
                $this->response(500, array("error" => "No password provided."));

            $this->user->setEmail($data["email"]);
            $this->user->setPassword($data["password"]);
            try {
                $result = $this->user->findByCredentials();
                $this->response(200, array("access_token" => $result));
            } catch (Exception $ex) {
                $msg = $ex->getMessage();
                $this->response(500, array("error" => $msg));
            }
        }
    }
}
