<?php

require_once 'app/Utils/RestfulAPI.php';
require_once 'app/config/Database.php';
require_once 'app/Models/Products.php';

class ProductsApi extends RestFul_API {

    private $database;
    private $db;
    private $products;

    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->products = new Products($this->db);
        parent::__construct();
    }

    function allproducts() {
        if ($this->method == "GET") {
            //get a product
            $proID = isset($_GET["id"]) ? $_GET["id"] : null;
            if ($proID) {
                $this->products->setID($proID);
                $proArr = $this->products->getaProduct();
                if (is_array($proArr)) {
                    $this->response(200, $proArr);
                } else
                    $this->response(404, $proArr);
            }
            //get all products
            $proArr = $this->products->getAllProducts();
            if (is_array($proArr)) {
                $this->response(200, $proArr);
            } else
                $this->response(404, $proArr);
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    //update product
    function updateproduct() {
        if ($this->method == "POST") {
            $proData = json_decode(file_get_contents('php://input'));
            if ($proData->pro_id) {
                $this->products->setID($proData->pro_id);
                $this->products->setName($proData->pro_name);
                $this->products->setQuantity($proData->pro_quantity);
                // $this->products->setCategories($proData->pro_categories);
                $this->products->setSaleprice($proData->pro_saleprice);
                $this->products->setPurchaseprice($proData->pro_purchaseprice);
                $updatePro = $this->products->updateProduct();
                if (isset($updatePro["msg"])) {
                    $this->response(200, $updatePro);
                } else {
                    $this->response(500, $updatePro);
                } 
            }            
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    //create a product
    function createproduct() {
        if ($this->method == "POST") {
            $proData = json_decode(file_get_contents('php://input'));
            if ($proData->pro_categories) {
                $this->products->setID($this->genProId($proData->pro_categories));
                $this->products->setName((string)$proData->pro_name);
                $this->products->setQuantity($proData->pro_quantity);
                $this->products->setCategories((string)$proData->pro_categories);
                $this->products->setSaleprice($proData->pro_saleprice);
                $this->products->setPurchaseprice($proData->pro_purchaseprice);
                $addPro = $this->products->createProduct();
                if (isset($addPro["msg"])) {
                    $this->response(200, $addPro);
                } else {
                    $this->response(500, $addPro);
                } 
            }            
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    //delete product
    function deleteproduct() {
        if ($this->method == "POST") {
            $proData = json_decode(file_get_contents('php://input'));
            if ($proData->pro_id) {
                $this->products->setID($proData->pro_id);
                $delPro = $this->products->deleteProduct();
                if (isset($delPro["msg"])) {
                    $this->response(200, $delPro);
                } else {
                    $this->response(500, $delPro);
                } 
            } 
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    //search product
    function searchproducts() {
        if ($this->method == "GET"){
            $sqlQuery = isset($_GET["q"]) ? $_GET["q"] : null;
            if ($sqlQuery) {
                $this->products->setName($sqlQuery);
                $this->products->setCategories($sqlQuery);
                $search_arr = $this->products->searchProduct();
                if (isset($search_arr["body"])) {
                    $this->response(200, $search_arr);
                } else
                    $this->response(500, $search_arr);
            }
            if ($query === "") {
                $this->response(500, array("error" => "no query provided."));
            }
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    private function genProId ($products_categories) {
        $this->products->setCategories($products_categories);
        $id = $this->products->genProID();
        return $id;
    }
}
?>