<?php

require_once 'app/Utils/RestfulAPI.php';
require_once 'app/config/Database.php';
require_once 'app/Models/ProCate.php';

class ProCatesApi extends RestFul_API {
    private $database;
    private $db;
    private $procates;

    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->procates = new ProCates($this->db);
        parent::__construct();
    }

    public function allcates() {
        if ($this->method == "GET") {
            //get all categories
            $catesArr = $this->procates->getAllCategories();
            if (is_array($catesArr)) {
                $this->response(200, $catesArr);
            } else
                $this->response(404, $catesArr);
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }

    public function createcate() {
        //request method: POST
        // request data form = {
        //     cateCode: "pork",
        //     cateDes: "thịt lợn"
        // }
        if ($this->method == "POST") {
            $proCateData = json_decode(file_get_contents('php://input'));
            if ($proCateData->cateCode) {
                $this->procates->setproCateCode($proCateData->cateCode);
                $this->procates->setproCateDes($proCateData->cateDes);
                $addCates = $this->procates->createCate();
                if (isset($addCates["msg"])) {
                    $this->response(200, $addCates);
                } else {
                    $this->response(500, $addCates);
                } 
            }            
        } else {
            $this->response(405, array("error" => "method not allowed"));
        }
    }
}
?>