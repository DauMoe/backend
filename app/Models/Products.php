<?php
require_once 'vendor/autoload.php';

// use Firebase\JWT\JWT;

class Products {  
    // Connection
    private $conn;

    // Columns
    private $pro_id, $pro_name, $pro_quantity, $pro_categories, $pro_saleprice, $pro_purchaseprice;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function setID($pro_id): void {
        $this->pro_id = $pro_id;
    }

    public function setName($pro_name): void {
        $this->pro_name = $pro_name;
    }

    public function setQuantity($pro_quantity): void {
        $this->pro_quantity = $pro_quantity;
    }

    public function setCategories($pro_categories): void {
        $this->pro_categories = $pro_categories;
    }

    public function setSaleprice($pro_saleprice): void {
        $this->pro_saleprice = $pro_saleprice;
    }

    public function setPurchaseprice($pro_purchaseprice): void {
        $this->pro_purchaseprice = $pro_purchaseprice;
    }

    //============================================================
    // Get all products
    public function getAllProducts() {
        $sqlQuery = "SELECT * FROM products";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $proArr = array();
                $proArr["body"] = array();
                $proArr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "pro_id"=>$pro_id, 
                        "pro_name"=>$pro_name, 
                        "pro_quantity"=>$pro_quantity, 
                        "pro_categories"=>$pro_categories, 
                        "pro_saleprice"=>$pro_saleprice, 
                        "pro_purchaseprice"=>$pro_purchaseprice
                    );
                    array_push($proArr["body"], $e);
                }
                return $proArr;
            } else {
                return "msg: No products";
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
    }

    //get a product
    public function getaProduct() {
        $sqlQuery = "SELECT * FROM products WHERE pro_id = :id";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindValue(":id", htmlspecialchars(strip_tags($this->pro_id)));
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $proArr = array();
                $proArr["body"] = array();
                $proArr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "pro_id"=>$pro_id, 
                        "pro_name"=>$pro_name, 
                        "pro_quantity"=>$pro_quantity, 
                        "pro_categories"=>$pro_categories, 
                        "pro_saleprice"=>$pro_saleprice, 
                        "pro_purchaseprice"=>$pro_purchaseprice
                    );
                    array_push($proArr["body"], $e);
                }
                return $proArr;
            } else {
                return "msg: No products";
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
    }

    //update product
    public function updateProduct() {
        $sqlQuery = "UPDATE products SET pro_name = :name, pro_quantity = :quantity, pro_saleprice = :saleprice, pro_purchaseprice = :purchaseprice WHERE pro_id = :id";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //Data binding
            $stmt->bindValue(":id", htmlspecialchars(strip_tags($this->pro_id)));
            $stmt->bindValue(":name", htmlspecialchars(strip_tags($this->pro_name)));
            $stmt->bindValue(":quantity", htmlspecialchars(strip_tags($this->pro_quantity)));
            // $stmt->bindValue(":categories", htmlspecialchars(strip_tags($this->pro_categories)));
            $stmt->bindValue(":saleprice", htmlspecialchars(strip_tags($this->pro_saleprice)));
            $stmt->bindValue(":purchaseprice", htmlspecialchars(strip_tags($this->pro_purchaseprice)));
            $stmt->execute();
            return array("msg" => "Edit products successful");
        } catch (PDOException $exc) {            
            return array("error" => $exc);
        }
    }

    //create product
    public function createProduct() {
        $sqlQuery = "INSERT INTO products (pro_id, pro_name, pro_quantity, pro_categories, pro_saleprice, pro_purchaseprice) VALUES (:id, :name, :quantity, :categories, :saleprice, :purchaseprice)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //Data binding
            $stmt->bindValue(":id", $this->testData($this->pro_id));
            $stmt->bindValue(":name", $this->testData($this->pro_name));
            $stmt->bindValue(":quantity", $this->testData($this->pro_quantity));
            $stmt->bindValue(":categories", $this->testData($this->pro_categories));
            $stmt->bindValue(":saleprice", $this->testData($this->pro_saleprice));
            $stmt->bindValue(":purchaseprice", $this->testData($this->pro_purchaseprice));
            $stmt->execute();
            return array("msg" => "Create products successful");
        } catch (PDOException $exc) {            
            return array("error" => $exc);
        }
    }

    //delete products
    public function deleteProduct() {
        $sqlQuery = "DELETE FROM products WHERE pro_id REGEXP '[[:<:]]".$this->testData($this->pro_id)."[[:>:]]'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return array("msg" => "product deleted successfully");
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }

    // Search Product
    function searchProduct() {
        $sqlQuery = "SELECT pro_name FROM products WHERE pro_name LIKE '%".$this->testData($this->pro_name)."%' OR pro_categories LIKE '%".$this->testData($this->pro_categories)."%'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);    
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $products_arr = array();
                $products_arr["body"] = array();
                $products_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "pro_name"=>$pro_name
                    );
                    array_push($products_arr["body"], $e);
                }
                return $products_arr;
            } else {
                return "msg: no products.";
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }


    //================= support function =====================
    public function genProID() {
        $sqlQuery = "SELECT pro_id FROM products WHERE pro_categories = :categories ORDER BY pro_id DESC LIMIT 1";
        $cateLeng = strlen($this->pro_categories)+1;
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindValue(":categories", htmlspecialchars(strip_tags($this->pro_categories)));
            $stmt->execute();
            $proArr = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $e = $pro_id;
                array_push($proArr, $e);
            }
            if (!empty($proArr)) {
                $nextID = (int)substr($proArr[0], $cateLeng)+1;
            } else {
                $nextID = 1;
            }           
            return $this->pro_categories."-".(string)$nextID;
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
    }

    private function testData($data) {
        return htmlspecialchars(strip_tags(trim($data)));
    }
}
?>
