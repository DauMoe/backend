<?php
require_once 'vendor/autoload.php';

// use Firebase\JWT\JWT;

class User
{  
    // Connection
    private $conn;

    // Columns
    private $id;
    private $password;
    private $name;
    private $phone;
    private $email;
    private $date;
    private $role;
    private $address;
    private $created;

    public function __construct($db)
    {
        $this->conn = $db;     
    }

    public function setID($id): void
    {
        $this->id = $id;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function setRole($role): void
    {
        $this->role = $role;
    }

    public function setAddress($address): void
    {
        $this->address = $address;
    }

    public function setCreated($created): void
    {
        $this->created = $created;
    }

    // Get all users
    public function getUsers()
    {
        $sqlQuery = "SELECT * FROM user";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $userArr = array();
                $userArr["body"] = array();
                $userArr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "phone" => $phone,
                        "password" => $password,
                        "date" => $date,
                        "role" => $role,
                        "address" => $address,
                        "created" => $created
                    );
                    array_push($userArr["body"], $e);
                }
                return $userArr;
            } else {
                return "Msg: No user.";
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
        //        
    }
    // Get single User
    function getSingleUser()
    {
        $sqlQuery = "SELECT id, password, name, email, date, role, created FROM User WHERE id = :id";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindValue(":id", htmlspecialchars(strip_tags($this->id)));
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $userArr = array();
                $userArr["body"] = array();
                $userArr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "phone" => $phone,
                        "password" => $password,
                        "date" => $date,
                        "role" => $role,
                        "address" => $address,
                        "created" => $created
                    );
                    array_push($userArr["body"], $e);
                }
                return $userArr;
            } else {
                return "Msg: No user.";
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
    }

    public function createUser()
    {
        $sqlQuery = "INSERT INTO User (name, phone, email, password, date, role, address, created) "
            . "VALUES (:name, :phone, :email, :password, :date, :role, :address, :created)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //        Data binding
            $stmt->bindValue(":name", htmlspecialchars(strip_tags($this->name)));
            $stmt->bindValue(":email", htmlspecialchars(strip_tags($this->email)));
            $stmt->bindValue(":phone", htmlspecialchars(strip_tags($this->phone)));

            $password_hashed = password_hash(htmlspecialchars(strip_tags($this->password)),PASSWORD_BCRYPT, ["cost" => 11]);
            $stmt->bindValue(":password", $password_hashed);
            
            $stmt->bindValue(":date", htmlspecialchars(strip_tags($this->date)));
            $stmt->bindValue(":role", htmlspecialchars(strip_tags($this->role)));
            $stmt->bindValue(":address", htmlspecialchars(strip_tags($this->address)));
            $stmt->bindValue(":created", htmlspecialchars(strip_tags($this->created)));
            $stmt->execute();
            return array("msg" => array("msg" => "Create user successful."), "status" => 200);
        } catch (PDOException $e) {
            return array("msg" => array("error" => $e->getMessage()), "status" => 500);
        }
    }

    // public function findByCredentials()
    // {
    //     $sqlQuery = "SELECT * FROM User WHERE email = :email";
    //     try {
    //         $stmt = $this->conn->prepare($sqlQuery);
    //         //        Data binding
    //         $stmt->bindValue(":email", htmlspecialchars(strip_tags($this->email)));
                       
    //         $stmt->execute();
    //         $row = $stmt->fetch(PDO::FETCH_ASSOC);
    //         if (!password_verify(htmlspecialchars(strip_tags($this->password)), $row["password"])) {
    //             throw new Exception("Invalid login credentials");
    //         }
    //         $accesstoken = $this->generateAccessToken(["userID" => $row["id"],
    //             "email" => $row["email"], "role" => $row["role"]], $_ENV["ACCESSTOKENKEY"], 60);
    //         return $accesstoken;
    //     } catch (PDOException $exc) {
    //         echo $exc->getTraceAsString();
    //     }
    // }

    // private function generateAccessToken($data, $key, $expTime)
    // {
    //     $tokenId = uniqid(rand(), true);
    //     $issuedAt = time();
    //     $notBefore = $issuedAt;
    //     $expire = $notBefore + $expTime;            // Adding expTime seconds
    //     $serverName = $_SERVER["SERVER_NAME"]; // Retrieve the server name from config file

    //     /*
    //      * Create the token as an array
    //      */

    //     $payload = [
    //         'iat' => $issuedAt, // Issued at: time when the token was generated
    //         'jti' => $tokenId, // Json Token Id: an unique identifier for the token
    //         'iss' => $serverName, // Issuer
    //         'nbf' => $notBefore, // Not before
    //         'exp' => $expire, // Expire
    //         'data' => $data
    //     ];

    //     $accessToken = JWT::encode($payload, $key);
    //     return $accessToken;
    // }
}
